import { PhonebookActions, PhonebookActionTypes } from '../actions/phonebook.actions';
import { Contact } from '../models/contact.interface';

const initialState: Contact = {
    name: 'John Peters',
    phoneNumber: '011-312-3444'
}

export function phonebookReducer(state: Contact[] = [initialState], action: PhonebookActions) {
    switch(action.type) {
        case PhonebookActionTypes.ADD_CONTACT:
            return [...state, action.payload];
        default:
            return state;
    }
}