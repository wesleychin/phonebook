
import { Contact } from '../../reactive/models/contact.interface';
import { Store } from '@ngrx/store';
import { AddContactAction } from '../actions/phonebook.actions';
import { AppState } from '../../../app.state';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PhonebookService {
    constructor(public store: Store<AppState>) {}

    public addContact(payload: Contact) {
        this.store.dispatch(new AddContactAction(payload));
    }

    public getContacts(): Observable<Contact[]> {
        return this.store.select('contacts');
    }    
}