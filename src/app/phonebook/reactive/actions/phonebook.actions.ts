import { Action } from '@ngrx/store';
import { Contact } from '../models/contact.interface';

export enum PhonebookActionTypes {
    ADD_CONTACT = '[Phonebook] Add contact'
}

export class AddContactAction implements Action {
    readonly type = PhonebookActionTypes.ADD_CONTACT;
    constructor(public payload: Contact) {}
}

export type PhonebookActions = AddContactAction;