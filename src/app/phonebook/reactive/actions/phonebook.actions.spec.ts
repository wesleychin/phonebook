import { PhonebookMockData } from "../../mock-data/phonebook-mock-data";
import { Contact } from '../models/contact.interface';
import { AddContactAction, PhonebookActionTypes } from './phonebook.actions';

describe('Given the phonebook actions', () => {
    const phonebookMockData: PhonebookMockData = new PhonebookMockData();

    describe('When a new AddContactAction is created', () => {
        it('Then it should create an action with type ADD_CONTACT with a payload of Contact', () => {
            const payload: Contact = phonebookMockData.getContact();

            const action = new AddContactAction(payload);
            
            expect({ ...action }).toEqual({
                type: PhonebookActionTypes.ADD_CONTACT,
                payload
            });
        });
    });
});