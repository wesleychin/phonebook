import { AppState } from "src/app/app.state";
import { Store, StoreModule } from '@ngrx/store';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { phonebookReducer } from '../../reactive/reducer/phonebook.reducer';
import { PhonebookContainerComponent } from './phonebook-container.component';
import { ListComponent } from '../../components/list/list.component';
import { AddContactComponent } from '../../components/add-contact/add-contact.component';
import { PhonebookService } from '../../reactive/services/phonebook.service';
import { MatDialog } from '@angular/material';

describe('Given the phonebook container component', () => {
    let component: PhonebookContainerComponent,
    fixture: ComponentFixture<PhonebookContainerComponent>,
    store: Store<AppState>,
    dialog: MatDialog;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          AngularMaterialModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          StoreModule.forRoot({
            phonebookReducer: phonebookReducer
          }),
        ],
        declarations: [
          PhonebookContainerComponent,
          ListComponent,
          AddContactComponent
        ],
        providers: [
          {
            provide: PhonebookService,
            useValue: jasmine.createSpyObj('PhonebookService', ['getContacts'])
          },
          {
            provide: MatDialog,
            useValue: jasmine.createSpyObj('MatDialog', ['open'])
          }
        ]
      }).compileComponents();
  
      store = TestBed.get(Store);
  
      spyOn(store, 'select').and.callThrough();
      dialog = TestBed.get(MatDialog);
    }));

    afterEach(() => {
      fixture.destroy();
      component = null;
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(PhonebookContainerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('Then it should create the phonebook container component', () => {
      const fixture = TestBed.createComponent(PhonebookContainerComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    });
  
    it('Then it should render Wesley Chin\'s Phonebook in a h1 tag', () => {
        const title = fixture.debugElement.nativeElement.querySelector('h1');
        expect(title.textContent).toEqual('Wesley Chin\'s Phonebook');
      });
    
      it('Then it should render the Add contact button', () => {
        const button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button).toBeTruthy();
      });
    
      it('Then the button\'s icon should be playlist_add', () => {
        const icon = fixture.debugElement.nativeElement.querySelector('mat-icon');
        expect(icon.textContent).toEqual('playlist_add');
      });
    
      it('Then the button\'s text should be Add contact', () => {
        const button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.textContent).toContain('Add contact');
      });

      it('Then it should render the list-component component', () => {
        const listComponent = fixture.debugElement.nativeElement.querySelector('list-component');
        expect(listComponent).toBeTruthy();
      });

      describe('When the user clicks the Add contact button', () => {
        beforeEach(() => {
          const button = fixture.debugElement.nativeElement.querySelector('button');
          button.click();
        });

        it('then the add contact component should be opened in a dialog', () => {
          expect(dialog.open).toHaveBeenCalledWith(AddContactComponent, {
            width: '45%'
          });
        });
      })
  });
  