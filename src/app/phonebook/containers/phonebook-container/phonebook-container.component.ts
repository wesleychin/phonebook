import {
    Component,
    OnInit
  } from '@angular/core';
  import { MatDialog } from '@angular/material';
import { AddContactComponent } from '../../components/add-contact/add-contact.component';
import { Contact } from '../../reactive/models/contact.interface';
import { Observable } from 'rxjs';
import { PhonebookService } from '../../reactive/services/phonebook.service';
  
  @Component({
    selector: 'phonebook-container',
    templateUrl: './phonebook-container.component.html',
    styleUrls: ['./phonebook-container.component.scss']
  })
  export class PhonebookContainerComponent implements OnInit {
    contacts$: Observable<Contact[]>;
    
    constructor(public dialog: MatDialog, public phonebookService: PhonebookService) {
    }
  
    ngOnInit() {
      this.contacts$ = this.phonebookService.getContacts();
    }

    openDialog(): void {
        this.dialog.open(AddContactComponent, {
          width: '45%'
        });
      }
  }
  