import { Injectable } from '@angular/core';
import { Contact } from './../reactive/models/contact.interface';

@Injectable()
export class PhonebookMockData {
    constructor() {}

    getContacts(): Contact[] {
        return [
                {name: 'John Peters', phoneNumber: '011-123-4567'},
                {name: 'Craig Michaels', phoneNumber: '072-345-6789'}
            ];
    }

    getContact(): Contact {
        return this.getContacts[0];
    }
}