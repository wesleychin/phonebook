import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Contact } from '../../reactive/models/contact.interface';

export interface PeriodicElement {
  name: string;
  phoneNumber: string;
}

@Component({
    selector: 'list-component',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges {
  @Input() contacts: Contact[];
  displayedColumns: string[] = ['name', 'phoneNumber'];
  dataSource = new MatTableDataSource<Contact>();

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.dataSource.data = this.contacts;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
