import { Component, OnInit } from '@angular/core';
  
import { MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { PhonebookService } from '../../reactive/services/phonebook.service';

  @Component({
    selector: 'add-contact-component',
    templateUrl: './add-contact.component.html',
    styleUrls: ['./add-contact.component.scss']
  })
  export class AddContactComponent implements OnInit {
    constructor(
      public formBuilder: FormBuilder,
      public dialogRef: MatDialogRef<AddContactComponent>,
      public dialog: MatDialog,
      public phonebookService: PhonebookService
    ) {}

    addContactFormGroup: FormGroup;
  
    ngOnInit() {
      this.addContactFormGroup = this.formBuilder.group({
        name: ['', [Validators.required]],
        phoneNumber: ['', [Validators.required]]
      });
    }

    addContact() {
      const name = this.addContactFormGroup.get('name').value;
      const phoneNumber = this.addContactFormGroup.get('phoneNumber').value;

      this.phonebookService.addContact({name: name, phoneNumber: phoneNumber});
      
      this.dialogRef.close();
    }
  
    close() {
      this.dialogRef.close();
    }
  }