import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PhonebookContainerComponent } from './containers/phonebook-container/phonebook-container.component';
import { ListComponent } from './components/list/list.component';
import { AddContactComponent } from './components/add-contact/add-contact.component';

@NgModule({
    imports: [
      CommonModule,
      AngularMaterialModule,
      ReactiveFormsModule
    ],
    declarations: [
      PhonebookContainerComponent,
      ListComponent,
      AddContactComponent
    ],
    exports: [PhonebookContainerComponent],
    entryComponents: [AddContactComponent]
  })
  export class PhoneBookModule {}
  