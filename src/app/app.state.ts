import { Contact } from './phonebook/reactive/models/contact.interface';

export interface AppState {
    readonly contacts: Contact[];
}