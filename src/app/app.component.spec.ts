import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { PhonebookContainerComponent } from './phonebook/containers/phonebook-container/phonebook-container.component';
import { ListComponent } from './phonebook/components/list/list.component';
import { AddContactComponent } from './phonebook/components/add-contact/add-contact.component';
import { AngularMaterialModule } from './angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PhonebookService } from './phonebook/reactive/services/phonebook.service';
import { Store, StoreModule } from '@ngrx/store';
import { AppState } from './app.state';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { phonebookReducer } from './phonebook/reactive/reducer/phonebook.reducer';

describe('Given the app component', () => {
  let component: AppComponent,
  fixture: ComponentFixture<AppComponent>,
  store: Store<AppState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AngularMaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({
          phonebookReducer: phonebookReducer
        })
      ],
      declarations: [
        AppComponent,
        PhonebookContainerComponent,
        ListComponent,
        AddContactComponent
      ],
      providers: [
        {
          provide: PhonebookService,
          useValue: jasmine.createSpyObj('PhonebookService', ['getContacts'])
        }
      ]
    }).compileComponents();

    store = TestBed.get(Store);

    spyOn(store, 'select').and.callThrough();
  }));

  afterEach(() => {
    fixture.destroy();
    component = null;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Then it should create the app component', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`Then it should have as title 'phonebook'`, () => {
    const title = fixture.debugElement.componentInstance.title;
    expect(title).toEqual('phonebook');
  });

  it('Then it should render the phonebook-component component', () => {
    const phonebookContainer = fixture.debugElement.nativeElement.querySelector('phonebook-container');
    expect(phonebookContainer).toBeTruthy();
  });
});
