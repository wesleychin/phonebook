import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularMaterialModule } from './angular-material.module';

import { PhoneBookModule } from './phonebook/phonebook.module';
import { StoreModule, ActionReducer, State } from '@ngrx/store';
import { phonebookReducer } from './phonebook/reactive/reducer/phonebook.reducer';
import { storeLogger } from 'ngrx-store-logger';
import { AppState } from './app.state';
import { PhonebookService } from './phonebook/reactive/services/phonebook.service';

export function ngrxStoreLogger(reducer: ActionReducer<AppState>): any {
  return storeLogger()(reducer);
}

export const metaReducers = [ngrxStoreLogger];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    PhoneBookModule,
    StoreModule.forRoot({
      contacts: phonebookReducer
    },
    {metaReducers})
  ],
  providers: [PhonebookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
